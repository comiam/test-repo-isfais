import pytest

from source_code import Comparator


@pytest.mark.parametrize("test_input, expected",
                         [
                             ((-1, 1), False),
                             ((-10, -10), True),
                             ((5, 5), True),
                             ((30932, 19383), False),
                             ((30932.90003, 30932.900031), False)
                         ])
def test_equls(test_input, expected):
    comp = Comparator(*test_input)

    assert comp.is_equals() == expected


@pytest.mark.parametrize("init_vals, new_vals",
                         [
                             ((-1, 1), (-10, 9)),
                             ((-10, -10), (0, 0)),
                             ((5, 5), (4, 8)),
                             ((30932, 19383), (1e-6, 1e-8)),
                             ((30932.90003, 30932.900031), (92483.43432, 394.4938))
                         ])
def test_set_ab(init_vals, new_vals):
    comp = Comparator(*init_vals)
    comp.set_ab(*new_vals)
    assert (comp.a == new_vals[0]) and (comp.b == new_vals[1])
