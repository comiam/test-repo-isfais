# Use the official Python image
FROM python:3.8

# Set the working directory in the container
WORKDIR /app

# Copy the source code into the container
COPY . /app

# Install dependencies
RUN pip install --no-cache-dir -r requirements.txt

# Run the tests
CMD ["pytest", "modules_test.py"]
