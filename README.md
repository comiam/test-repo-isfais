# Содержание

- [Описание CI-скрипта для GitLab](#описание-ci-скрипта-для-gitlab)
  - [Общее описание](#общее-описание)
  - [.gitlab-ci](#gitlab-ci)
- [Stages](#stages)
  - [Шаблон для задач с Python](#шаблон-для-задач-с-python)
  - [Общий шаблон задачи](#общий-шаблон-задачи)
  - [Linting (статический анализ кода)](#linting-статический-анализ-кода)
  - [Тестирование](#тестирование)
  - [Сборка Docker-образа](#сборка-docker-образа)
  - [Deploy нашего кода *воображаемый*](#deploy-нашего-кода-воображаемый)
  - [Параллельная задача](#параллельная-задача)
- [Общие вопросы](#общие-вопросы)
  - [В каком порядке будут идти stage?](#в-каком-порядке-будут-идти-stage)
  - [Запуск CI при коммите (push):](#запуск-ci-при-коммите-push)
  - [Запуск CI при мёрдже (merge request):](#запуск-ci-при-мёрдже-merge-request)
  - [Дополнительные условия с использованием `changes`:](#дополнительные-условия-с-использованием-changes)
  - [Что такое переменные?](#что-такое-переменные)
  - [Как переменные задавать?](#как-переменные-задавать)
  - [Базовые переменные GitLab](#базовые-переменные-gitlab)
  - [Что такое Gitlab Registry, в чём его отличие от Docker Hub?](#что-такое-gitlab-registry-в-чём-его-отличие-от-docker-hub)
  - [Что происходит в before\_script на build stage?](#что-происходит-в-before_script-на-build-stage)

# Описание CI-скрипта для GitLab

## Общее описание

Репозиторий с кратким пособием по GitLab CI. Ниже описание для .gitlab-ci.

## .gitlab-ci

Этот файл GitLab CI представляет собой конфигурацию для автоматизации процессов разработки, тестирования, сборки и развертывания программного продукта с использованием GitLab CI/CD. Давайте рассмотрим каждый этап и компоненты скрипта.

# Stages

## Шаблон для задач с Python

```yaml
.python_job:
  image: python:3.8
```

- `.python_job`: Шаблон задачи, определяющий базовый образ Python 3.8. Его могут наследовать другие задачи, которым необходимо, чтобы использовался образ python:3.8, в котором будет запускаться их скрипт.

## Общий шаблон задачи

```yaml
.common_job_template:
  extends: .python_job
  before_script:
    - pip install -r requirements.txt
  cache:
    key: common-pip-cache
    paths:
      - .cache/pip
    when: 'on_success'
```

- `.common_job_template`: Общий шаблон задачи, который наследует шаблон Python, устанавливает зависимости из файла `requirements.txt` и использует кэш pip для ускорения процесса.

Блок `cache` в контексте GitLab CI используется для кэширования результатов выполнения задачи, чтобы ускорить последующие запуски, уменьшив необходимость повторного выполнения долгих операций. В данном случае, блок `cache` используется для кэширования директории pip, где хранятся зависимости Python.

Давайте рассмотрим подробно каждый атрибут:

- `key: common-pip-cache`: Этот атрибут определяет ключ, по которому будет производиться идентификация кэша. Ключ должен быть уникальным для задачи или группы задач.

- `paths: - .cache/pip`: Здесь указывается список путей к директориям или файлам, которые следует кэшировать. В данном случае, это `.cache/pip`, что обычно является стандартной директорией для кэширования пакетов pip.

- `when: 'on_success'`: Этот атрибут определяет, когда использовать кэш. Значение `'on_success'` указывает, что кэш следует использовать только в случае успешного завершения предыдущего запуска этой задачи. Таким образом, если предыдущий запуск был неудачным, кэш не будет использован, и задача выполнится снова.

Этот блок `cache` помогает уменьшить время выполнения задачи, так как pip не будет каждый раз загружать все зависимости заново. Вместо этого он будет использовать закэшированные результаты предыдущих успешных запусков, что может быть особенно полезным в средах, где часто происходят изменения в коде, но зависимости остаются неизменными.


## Linting (статический анализ кода)

```yaml
lint:
  stage: lint
  extends: .common_job_template
  script:
    - flake8
```

- `lint`: Этап, на котором производится статический анализ кода с использованием инструмента `flake8`.

## Тестирование

```yaml
test:
  stage: test
  extends: .common_job_template
  script:
    - pytest modules_test.py
```

- `test`: Этап, на котором выполняются тесты с использованием фреймворка `pytest`.

## Сборка Docker-образа

```yaml
build:
  stage: build
  image: docker:latest
  services:
    - docker:dind
  before_script:
    - echo "$CI_REGISTRY_PASSWORD" | docker login $CI_REGISTRY --username $CI_REGISTRY_USER --password-stdin
  script:
    - docker build -t ${CI_REGISTRY_IMAGE}:${CI_PIPELINE_IID} -t ${CI_REGISTRY_IMAGE}:latest .
    - docker push ${CI_REGISTRY_IMAGE}:latest
    - docker push ${CI_REGISTRY_IMAGE}:${CI_PIPELINE_IID}
```

- `build`: Этап, на котором осуществляется сборка Docker-образа и его отправка в GitLab Container Registry.

В контексте GitLab CI, блок `services` используется для определения дополнительных сервисов, которые должны быть запущены вместе с контейнером задачи. Ключевое слово `docker:dind` в этом контексте означает "Docker-in-Docker" сервис, который предоставляет возможность запускать Docker-контейнеры внутри другого Docker-контейнера.

## Deploy нашего кода *воображаемый*

```yaml
deploy:
  stage: deploy
  extends: .python_job
  script:
    # Замените этот шаг на реальные команды развертывания
    - echo "Deployment steps go here. Use shell Gitlab runner on server or run helm chart commands or simply transfer files!"
  only:
    - main
  when: manual
```

- `deploy`: Этап, на котором предполагается выполнение команд развертывания. Ручной запуск только для ветки `main`.

Вручную его можно запустить, зайдя на панель пайплайна, можно зайти через главную страницу репозитория, выбрав нужную ветку:

![Alt text](./images/1.png)

Затем нажав на кнопку со статусом пайплайна:

![Alt text](./images/2.png)

Затем нажав на кнопку запуска пайплайна:

![Alt text](./images/3.png)

## Параллельная задача

```yaml
job-parallel:
  stage: deploy
  variables:
    STAGE_VARIABLE: stage_value
  script:
    - echo "Let's do something interesting! We will steal variables from the logs!"
    - echo "TEST_VARIABLE value = $TEST_VARIABLE"
    - echo "STAGE_VARIABLE value = $STAGE_VARIABLE"
    - echo "TEST_HIDDEN_VARIABLE value = $TEST_HIDDEN_VARIABLE"
  only:
    - main
```

- `job-parallel`: Дополнительная задача, выполняемая параллельно с развертыванием. Выполняется только для ветки `main`.

# Общие вопросы

## В каком порядке будут идти stage?

Этапы (stage) выполняются в порядке, указанном в секции `stages`. В данном случае порядок следующий: lint, test, build, deploy.

## Запуск CI при коммите (push):

В GitLab CI/CD, задачи могут автоматически запускаться при каждом коммите (push) в репозиторий. Для этого достаточно создать файл `.gitlab-ci.yml` в корне вашего проекта и определить этапы и задачи, которые вы хотите выполнять.

Пример:

```yaml
stages:
  - build
  - test

build:
  stage: build
  script:
    - echo "Building the project..."

test:
  stage: test
  script:
    - echo "Running tests..."
```

В этом примере, задачи `build` и `test` будут выполняться при каждом коммите в репозиторий.

## Запуск CI при мёрдже (merge request):

Чтобы запустить CI при создании мёрдж-реквеста, вы можете использовать ключевое слово `only` с параметром `merge_requests`. Также, вы можете настроить условия выполнения с помощью других ключевых слов, таких как `changes`.

Пример:

```yaml
stages:
  - test

test:
  stage: test
  script:
    - echo "Running tests..."
  only:
    - merge_requests
```

В этом примере, задача `test` будет выполняться только при создании мёрдж-реквеста.

## Дополнительные условия с использованием `changes`:

```yaml
stages:
  - test

test:
  stage: test
  script:
    - echo "Running tests..."
  only:
    - merge_requests
  changes:
    - "*.py"
```

В этом примере, задача `test` будет выполняться при создании мёрдж-реквеста, только если изменены файлы с расширением `.py`.

## Что такое переменные?

В GitLab CI переменные — это способ хранения и использования значений, которые могут быть использованы в ваших CI/CD скриптах. Они предоставляют удобный механизм для передачи конфиденциальной информации, параметров конфигурации, или любых других значений, которые ваш пайплайн может использовать.

В GitLab CI переменные бывают нескольких типов:

1. **Переменные окружения (Environment Variables):** Эти переменные доступны в каждом шаге вашего пайплайна. Они могут быть использованы в скриптах, настройках и т.д.

2. **Секреты (Secret Variables):** Секреты — это частный тип переменных, предназначенных для хранения конфиденциальной информации, такой как токены доступа, пароли и прочее. Они зашифрованы и недоступны в логах, что делает их безопасными для использования в конфигурациях CI/CD.

Пример использования переменных в `.gitlab-ci.yml`:

```yaml
stages:
  - deploy

deploy:
  stage: deploy
  script:
    - echo "Deploying to production server"
  only:
    - master
  environment:
    name: production
  variables:
    DATABASE_URL: "postgres://user:password@production-db:5432/mydatabase"
    API_KEY: "$CI_JOB_TOKEN"
```

В этом примере:

- `DATABASE_URL` - это переменная окружения.
- `API_KEY` - это переменная окружения, в которую записывается значение `$CI_JOB_TOKEN`, предоставляемое GitLab CI.

Переменные можно задавать и для всего проекта через интерфейс GitLab (Project Settings -> CI / CD -> Variables). Они могут быть использованы в пайплайнах, но будьте осторожны с конфиденциальной информацией, используемой в секретах, чтобы она не попала в логи и выводы пайплайна.

Чтобы переменная была скрыта, её надо пометить как masked variable. Пример работы скрытия переменной прекрасно продемонстрирован в `job-parallel`.

Здесь отображены `TEST_VARIABLE`, `STAGE_VARIABLE` и `TEST_HIDDEN_VARIABLE`. `STAGE_VARIABLE` задана в самой конфигурации CI, но две остальные - в переменных репозитория:

![Alt text](./images/4.png)

![Alt text](./images/5.png)

![Alt text](./images/6.png)

Здесь `TEST_HIDDEN_VARIABLE` помечена как скрытая:

![Alt text](./images/7.png)

Что и доказывает лог из `job-parallel`:

![Alt text](./images/8.png)

Так же в проекте есть такие переменные, что видны всем job:

```yaml
variables:
  CI_REGISTRY_IMAGE: $CI_REGISTRY/comiam/test-repo-isfais
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
```

- `CI_REGISTRY_IMAGE`: Переменная, содержащая путь к Docker-образу в GitLab Container Registry.
- `PIP_CACHE_DIR`: Переменная, определяющая директорию для кэша pip.

`CI_REGISTRY_IMAGE` используется для формирования пути к Docker-образу в GitLab Container Registry.

Давайте рассмотрим, как это работает:

- `$CI_REGISTRY`: Это встроенная переменная GitLab, которая представляет собой адрес Container Registry для данного проекта.
- `/comiam/test-repo-isfais`: Это часть пути к Docker-образу в реестре. Здесь `comiam` - это имя группы проекта (namespace), а `test-repo-isfais` - именем проекта.

Такая структура пути обычно используется для организации и категоризации Docker-образов внутри Container Registry. Она помогает логически группировать образы, делая удобным их управление и поиск внутри реестра.

Таким образом, `CI_REGISTRY_IMAGE` будет представлять полный путь к Docker-образу в GitLab Container Registry для данного проекта.

## Как переменные задавать?

Переменные могут быть заданы в блоке `variables` с использованием синтаксиса `$VARIABLE_NAME` или в блоке `script` при выполнении конкретных команд.

## Базовые переменные GitLab

GitLab предоставляет ряд встроенных переменных, таких как:
- `$CI_COMMIT_REF_NAME`: Название ветки.
- `$CI_COMMIT_SHA`: Хэш последнего коммита.
- `$CI_PIPELINE_IID`: Уникальный идентификатор конкретного пайплайна.
- и многие другие.

## Что такое Gitlab Registry, в чём его отличие от Docker Hub?

GitLab Container Registry - это встроенный сервис для хранения и управления Docker-образами. Отличие от Docker Hub заключается в том, что GitLab Container Registry интегрирован с GitLab, обеспечивая централизованное хранение образов внутри GitLab экосистемы.

## Что происходит в before_script на build stage?

В блоке `before_script` на этапе сборки (build stage) выполняются предварительные шаги перед выполнением основных команд. В данном случае:
- Выполняется логин в Docker Registry с использованием переменных окружения, таких как `$CI_REGISTRY_USER` и `$CI_REGISTRY_PASSWORD`.

Это необходимо для того, чтобы иметь права доступа к хранилищу Docker-образов в GitLab Container Registry перед их сборкой и публикацией.
