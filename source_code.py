from typing import Union
import logging

logging.basicConfig(level=logging.INFO)
numeric = Union[int, float, complex]


class Comparator:
    def __init__(self, a: numeric, b: numeric):
        """
        Class for comparing of a two numbers
        :param a: the first number
        :param b: the second number
        """

        self.a = a
        self.b = b

    def set_a(self, a: numeric):
        """
        Set the `a` (first) number
        :param a: new value for the first variable `a`
        """
        self.a = a

    def set_b(self, b: numeric):
        """
        Set the `b` (second) number
        :param a: new value for the first variable `b`
        """
        self.b = b

    def set_ab(self, a: numeric, b: numeric):
        """
        Set a and b numbers
        :param a: the first number
        :param b: the second number
        """
        self.a = a
        self.b = b

    def is_equals(self) -> bool:
        """
        Check a and b are equals or not
        :return: retrn boolan value, that mean  the currents numbear are equel or not
        """
        logging.info(f"Comparing of a following numbers: {self.a} and {self.b}")

        return self.a == self.b
